#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>
#include <QShortcut>
#include <QImage>
#include <QDebug>
#include <cmath>

/// Main class for the program.
///
/// This class contains the main functions for the program.
/// There are functions for merging two images together,
/// a function that creates a ghost version of the first image on the merge image,
/// and the functions for the GUI.

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    /// \fn MainWindow::MainWindow(QWidget *parent)
    /// \~English
    /// \brief Default Constructor for the MainWindow.
    /// \~Spanish
    /// \brief Constructor por defecto del MainWindow.
    explicit MainWindow(QWidget *parent = 0);

    /// \fn void MainWindow::MergeImages(const QImage &objectImage, const QImage &backgroundImage, QImage & mergedImage, int threshold, bool ghost, int x, int y, int offset_x, int offset_y)
    /// \~English
    /// \brief Funcion that merges two images together. It takes an image with any background color
    /// and puts it in another image that will act as a background. (Implements Greenscreen techonology)
    /// \~Spanish
    /// \brief Funcion que fusiona dos imagenes juntas. Toma una imagen con cualquier color de trasfondo
    /// y lo pone en otra imagen que actual como el  trasfondo. (Implementa la tecnologia de trasfondo verde)
    void MergeImages(const QImage &, const QImage &, QImage &, int, bool=false, int=0, int=0, int=0, int=0) ;

    /// \fn MainWindow::~MainWindow(QWidget *parent)
    /// \~English
    /// \brief Destructor for the MainWindow.
    /// \~Spanish
    /// \brief Destructor del MainWindow.
    ~MainWindow();

private slots:
    /// \fn void MainWindow::on_ghostBox_clicked()
    /// \~English
    /// \brief Funtion that invokes the thresholdSlider function and creates a
    /// ghost version of the image with greenscreen background.
    /// \~Spanish
    /// \brief Funcion que invoca la funcion thresholdSlider y crea una version
    /// fantasma de la imagen con el trasfondo verde.
    void on_ghostBox_clicked();

    /// \fn void MainWindow::on_btnSelectImage_clicked()
    /// \~English
    /// \brief Display the file browsing window to load an image. (Green Background)
    /// \~Spanish
    /// \brief Despliega la ventana de busqueda de archivos para subir una imagen. (Trasfondo verde)
    void on_btnSelectImage_clicked();

    /// \fn void MainWindow::on_btnSelectBackground_clicked()
    /// \~English
    /// \brief Display the file browsing window to load a background image.
    /// \~Spanish
    /// \brief Despliega la ventana de busqueda de archivos para subir una imagen para el trasfondo.
    void on_btnSelectBackground_clicked();

    /// \fn void MainWindow::on_btnMergeImages_clicked()
    /// \~English
    /// \brief  Invokes the function that merges the green screen image with the background image,
    /// then displays the merged image.
    /// \~Spanish
    /// \brief Invoca la funcion que fusional la imagen con transfondo verde con la imagen de transfondo,
    /// entonces displiega la imagen fusionada.
    void on_btnMergeImages_clicked();

    /// \fn void MainWindow::on_btnSaveImage_clicked()
    /// \~English
    /// \brief Displays the file browse window to choose the path to save
    /// the merged image in the file system.
    /// \~Spanish
    /// \brief Despliega la ventana de busqueda de archivos para escoger un camino
    /// guardar la imagen fusionada en el sistema de archivos.
    void on_btnSaveImage_clicked();

    /// \fn void MainWindow::Mouse_Pressed()
    /// \~English
    /// \brief Function that gets the coordinates of the image with the greenscreen.
    /// \~Spanish
    /// \brief Funcion que obtiene las coordenada de la imagen con el trasfondo verde.
    void Mouse_Pressed();

    /// \fn void MainWindow::Mouse_Pressed()
    /// \~English
    /// \brief Function that gets the coordinaates of the image with the final background.
    /// \~Spanish
    /// \brief Funcion que obtiene las coordenada de la imagen con el trasfondo final.
    void Mouse_PressedBackground() ;

    /// \fn void MainWindow::on_thresholdSlider_actionTriggered()
    /// \~English
    /// \brief Invokes the function that merges the green screen image with the background image.
    /// \~Spanish
    /// \brief Invoca la funcion que fusiona la imagen de trasfondo verde con la imagen del trasfondo final.
    void on_thresholdSlider_actionTriggered();

private:
    Ui::MainWindow *ui;
    QImage originalImage;
    QImage backgroundImage;
    QImage mergedImage;
};

#endif // MAINWINDOW_H
